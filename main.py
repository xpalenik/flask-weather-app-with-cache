import python_weather
import asyncio
import os
from datetime import datetime

CITY = 'Prague'
LAST = None
weather = None


async def getweather():
    global LAST
    global weather

    # declare the client. format defaults to the metric system (celcius, km/h, etc.)
    client = python_weather.Client(format=python_weather.METRIC)

    if not LAST or (abs(datetime.now() - LAST)).seconds > 5 * 60:
        print('Asking server using API')
        # fetch a weather forecast from a city
        weather = await client.find(CITY)
        LAST = datetime.now()

    # returns the current day's forecast temperature (int)
    print('Temperature in', CITY, ':', weather.current.temperature, '℃')

    # get the weather forecast for a few days
    for forecast in weather.forecasts:
        print(str(forecast.date), forecast.sky_text, forecast.temperature)

    print()

    # close the wrapper once done
    await client.close()


if __name__ == "__main__":
    # see https://stackoverflow.com/questions/45600579/asyncio-event-loop-is-closed-when-getting-loop
    # for more details
    if os.name == "nt":
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

    asyncio.run(getweather())
    asyncio.run(getweather())
